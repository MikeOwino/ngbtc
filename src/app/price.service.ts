import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { interval } from 'rxjs';
import { environment } from '../environments/environment';

interface CoinGeckoResponse {
  bitcoin: { usd: number };
}

@Injectable({
  providedIn: 'root',
})
export class PriceService {
  constructor(private http: HttpClient) {
    // Send price update every minute
    interval(600).subscribe(() => {
      this.updatePrice();
    });
  }

  updatePrice() {
    // Get the current price of BTC from CoinGecko API
    this.http
      .get<CoinGeckoResponse>(
        'https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd'
      )
      .subscribe((data) => {
        const btcPrice = data.bitcoin.usd;

        // Send the price to Telegram
        this.http
          .post(
            `https://api.telegram.org/bot${environment.telegramToken}/sendMessage`,
            {
              chat_id: environment.telegramChatId,
              text: `Current BTC price: $${btcPrice}`,
            }
          )
          .subscribe();
      });
  }
}
